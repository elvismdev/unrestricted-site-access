<?php
/*
Plugin Name: UnRestricted Site Access
Version: 0.1
Description: Excludes for the plugin <strong>Restricted Site Access</strong>
Author: Elvis Morales
Author URI: https://twitter.com/n3rdh4ck3r
Plugin URI: https://bitbucket.org/grantcardone/unrestricted-site-access
Text Domain: unrestricted-site-access
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

// Exclude API requests from restriction
add_filter('restricted_site_access_is_restricted', 'usa_api_override', 10, 2 );
function usa_api_override( $is_restricted, $wp ) {
    // check query variables to see if this is the api
	if ( ! empty( $wp->query_vars['json'] ) ) {
		$is_restricted = false;
	}
	return $is_restricted;
}
